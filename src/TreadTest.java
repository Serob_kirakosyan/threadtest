public class TreadTest {
    String name;
    int totalTime;

    public TreadTest(String name, int totalTime) {
        this.name = name;
        this.totalTime = totalTime;
    }

    public int subtractTime(int time) {

        if (time > totalTime) {
            int result = time - totalTime;
            totalTime = 0;
            return result;
        } else {
            totalTime -= time;
            return 0;
        }
    }

    public void printTime() {
        System.out.println(this.name + " " + totalTime);
    }
}
