
import java.util.ArrayList;
import java.util.List;

public class Test {
    static List<TreadTest> list = new ArrayList<TreadTest>();


    static int firstTime;

    static int secondTime;
    static int thirdTime;
    static int percent;
    static int fullTime;
    static int providedTimeByPercentFromFullTime;
    static int providedTimeByPercentFromBiggestTime;
    static int providedTimeByPercentFromSmallestTime;

    static {
        firstTime = 100;
        secondTime = 200;
        thirdTime = 300;
        percent = 5;
        fullTime = firstTime + secondTime + thirdTime;
        providedTimeByPercentFromFullTime = fullTime * percent / 100;
        providedTimeByPercentFromBiggestTime = thirdTime * percent / 100;
        providedTimeByPercentFromSmallestTime = firstTime * percent / 100;
    }


    public static void main(String[] args) {

        list.add(new TreadTest("First", 100));
        list.add(new TreadTest("Second", 200));
        list.add(new TreadTest("Third", 300));
        simpleFirst();
}


    private static void simpleFirst() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).printTime();
            int result = list.get(i).subtractTime(providedTimeByPercentFromFullTime);
            if (result == 0) {
                fullTime -= providedTimeByPercentFromFullTime;
            } else {
                fullTime += result;
                list.remove(i);
            }

            if (i == list.size() - 1) {
                i = -1;
            }
        }
        System.out.println("Full Time : " + fullTime);
    }
}














